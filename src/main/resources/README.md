# config/policy 文件说明，只适用于Linux环境
## ops.execute：执行命令配置，支持执行操作包括sql、shell、curl接口。
## ops.after：执行后同步修复配置，支持同步包括sql、shell、curl接口。
## 以下是各项配置说明
1. id：执行任务唯一ID
2. jdbc：数据库jdbc连接地址
3. username：数据库账号
4. password：数据库密码，同步修改密码后，这里会被修改
5. driver：数据库驱动
6. cron：执行任务定时器表达式，0或者空启动时执行一遍
7. test：测试是否连接成功，启动时执行一遍
8. sqls：执行SQL数组，每条命令一行
9. shells：执行shell脚本命令，每条命令一行，$0为原值，$1为修改后值