package com.idb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IdbOpsApplication {
    public static void main(String[] args) {
        SpringApplication.run(IdbOpsApplication.class);
    }
}
