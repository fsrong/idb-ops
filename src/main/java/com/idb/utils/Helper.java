package com.idb.utils;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.text.StrBuilder;
import cn.hutool.core.util.StrUtil;

public class Helper {
    /*
    生成密码
     */
    public static String generatePassword(String prefix,String suffix) {
        StrBuilder builder = StrBuilder.create(StrUtil.emptyToDefault(prefix,"Abs!*#"));
        builder.append(DateUtil.current());
        builder.append(StrUtil.emptyToDefault(suffix,"$"));
        return builder.toString();
    }
}
