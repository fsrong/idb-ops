package com.idb.executors;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.text.StrBuilder;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class NameCmd {
    private String cmd;
    private Password password;
    private boolean use;
    private Map<String,Object> paramMap = new HashMap<>();

    public NameCmd(String cmd, Password password) {
        this.password = password;
        this.parse(cmd);
    }

    private void parse(String namedSql) {
        this.use = namedSql.contains("$1");
        this.cmd = namedSql.replaceAll("$0", this.password.getOldPassword()).replaceAll("$1",this.password.getNewPassword());
        this.paramMap.put("$0",this.password.getOldPassword());
        this.paramMap.put("$1",this.password.getNewPassword());
    }

    @Setter
    @Getter
    public static class Password {
        String oldPassword;
        String newPassword;

        public Password(String oldPassword, String newPassword) {
            this.oldPassword = oldPassword;
            this.newPassword = newPassword;
        }
    }

}
