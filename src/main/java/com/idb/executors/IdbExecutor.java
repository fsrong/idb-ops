package com.idb.executors;

import cn.hutool.core.lang.Singleton;
import cn.hutool.db.handler.RsHandler;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

public interface IdbExecutor {
    default String execute(String cmd) {
        return null;
    }
    default <T> T query(Connection connection, String cmd, Map<String,Object> paramMap, RsHandler<T> rsHandler) {
        return null;
    }
    default int execute(Connection connection, String cmd, Map<String,Object> paramMap) {
        return 0;
    }
}
