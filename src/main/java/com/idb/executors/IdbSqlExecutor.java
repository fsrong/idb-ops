package com.idb.executors;

import cn.hutool.core.lang.Singleton;
import cn.hutool.db.handler.RsHandler;
import cn.hutool.json.JSONUtil;
import cn.hutool.log.Log;
import com.idb.utils.IdbSql;

import java.sql.Connection;
import java.util.Map;

public class IdbSqlExecutor implements IdbExecutor{
    private static final Log log  = Log.get();
    private IdbSqlExecutor() {
    }

    public static IdbExecutor getInstance() {
        return Singleton.get(IdbSqlExecutor.class);
    }


    @Override
    public <T> T query(Connection connection, String cmd, Map<String, Object> paramMap, RsHandler<T> rsHandler) {
        log.info(">>>>sql-command:{};sql-param:{}",cmd, JSONUtil.toJsonStr(paramMap));
        return IdbSql.getInstance().query(connection,cmd,paramMap,rsHandler);
    }

    @Override
    public int execute(Connection connection, String cmd, Map<String, Object> paramMap) {
        log.info(">>>>sql-command:{};sql-param:{}",cmd, JSONUtil.toJsonStr(paramMap));
        return IdbSql.getInstance().execute(connection,cmd,paramMap);
    }
}
