package com.idb.executors;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.lang.Singleton;
import cn.hutool.json.JSONUtil;
import cn.hutool.log.Log;

import java.io.*;
import java.util.Map;
import java.util.stream.Collectors;

public class ShellExecutor implements IdbExecutor {
    private static final Log log  = Log.get();
    private ShellExecutor() {
    }

    public static IdbExecutor getInstance() {
        return Singleton.get(ShellExecutor.class);
    }

    @Override
    public String execute(String cmd) {
        log.info(">>>>shell-command:{}",cmd);
        try {
            Process process = new ProcessBuilder().command(cmd).redirectErrorStream(true).start();
            return new BufferedReader(new InputStreamReader(process.getInputStream())).lines().collect(Collectors.joining(System.lineSeparator()));
        } catch (IOException e) {
            log.error("执行shell命令出错",e);
            throw new RuntimeException(e);
        }
    }
}
