package com.idb.dataSource;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.db.DbRuntimeException;
import cn.hutool.db.dialect.DriverUtil;
import cn.hutool.db.ds.simple.AbstractDataSource;
import cn.hutool.setting.dialect.Props;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.util.Properties;

public class IdbDataSource extends AbstractDataSource {
    private String driver;
    private String url;
    private String user;
    private String pass;

    private Properties connProps;

    public IdbDataSource() {

    }
    public void init(String url, String user, String pass) {
        this.init(url, user, pass, (String)null);
    }

    public void init(String url, String user, String pass, String driver) {
        this.driver = StrUtil.isNotBlank(driver) ? driver : DriverUtil.identifyDriver(url);

        try {
            Class.forName(this.driver);
        } catch (ClassNotFoundException var6) {
            throw new DbRuntimeException(var6, "Get jdbc driver [{}] error!", driver);
        }

        this.url = url;
        this.user = user;
        this.pass = pass;
    }

    /**
     * Closes this stream and releases any system resources associated
     * with it. If the stream is already closed then invoking this
     * method has no effect.
     *
     * <p> As noted in {@link AutoCloseable#close()}, cases where the
     * close may fail require careful attention. It is strongly advised
     * to relinquish the underlying resources and to internally
     * <em>mark</em> the {@code Closeable} as closed, prior to throwing
     * the {@code IOException}.
     *
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void close() throws IOException {

    }

    /**
     * <p>Attempts to establish a connection with the data source that
     * this {@code DataSource} object represents.
     *
     * @return a connection to the data source
     * @throws SQLException        if a database access error occurs
     * @throws SQLTimeoutException when the driver has determined that the
     *                             timeout value specified by the {@code setLoginTimeout} method
     *                             has been exceeded and has at least tried to cancel the
     *                             current database connection attempt
     */
    @Override
    public Connection getConnection() throws SQLException {
        Props info = new Props();
        if (this.user != null) {
            info.setProperty("user", this.user);
        }

        if (this.pass != null) {
            info.setProperty("password", this.pass);
        }

        Properties connProps = this.connProps;
        if (MapUtil.isNotEmpty(connProps)) {
            info.putAll(connProps);
        }

        return DriverManager.getConnection(this.url, info);
    }

    /**
     * <p>Attempts to establish a connection with the data source that
     * this {@code DataSource} object represents.
     *
     * @param username the database user on whose behalf the connection is
     *                 being made
     * @param password the user's password
     * @return a connection to the data source
     * @throws SQLException        if a database access error occurs
     * @throws SQLTimeoutException when the driver has determined that the
     *                             timeout value specified by the {@code setLoginTimeout} method
     *                             has been exceeded and has at least tried to cancel the
     *                             current database connection attempt
     * @since 1.4
     */
    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return DriverManager.getConnection(this.url, username, password);
    }
}
