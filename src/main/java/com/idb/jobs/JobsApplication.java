package com.idb.jobs;

import com.idb.listener.IdbPluginListener;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@AutoConfigureAfter(IdbPluginListener.class)
@Configuration
@EnableScheduling //开启定时任务
public class JobsApplication implements ApplicationContextAware {
    @Autowired
    private Jobs jobs;
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        jobs.start();
    }
}
