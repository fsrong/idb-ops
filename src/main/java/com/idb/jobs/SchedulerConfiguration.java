package com.idb.jobs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@Configuration
public class SchedulerConfiguration {
    @Autowired
    private Environment environment;
    @Bean
    public ThreadPoolTaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(Integer.parseInt(environment.getProperty("ops.scheduler.pool", "1")));
        scheduler.setRemoveOnCancelPolicy(true);
        scheduler.setThreadNamePrefix("jobs_");
        return scheduler;
    }
}
