package com.idb.yml;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.idb.annotations.Path;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Path("config/policy/ops.yml")
public class OpsYml {
    private List<Ops> ops = new ArrayList<>();
    @Getter
    @Setter
    public static class Ops{
        private ExecuteYml execute = new ExecuteYml();
        private ExecuteYml after = new ExecuteYml();
    }

    @Setter
    @Getter
    public static class ExecuteYml {
        private String id;
        private String jdbc;
        private String username;
        private String password;
        private String driver;
        private String cron;
        private String test;
        private Policy policy;
        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private List<String> sqls = new ArrayList<>();
        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private List<String> shells = new ArrayList<>();


        @Setter
        @Getter
        public static class Policy {
            private String prefix;
            private String suffix;
        }
    }
}
