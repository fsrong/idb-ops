package com.idb.yml;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.idb.annotations.Path;
import com.idb.utils.YmlUtils;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Path("config/.logs.yml")
public class LogYml {
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Log> logs = new ArrayList<>();
    @Setter
    @Getter
    public static class Log {
        private String id;
        private String lastTime;
        private boolean success;
    }

    public static void main(String[] args) {
        try {
            LogYml logYml = YmlUtils.load("config/.logs.yml",LogYml.class);
            YmlUtils.dump("config/.logs.yml",logYml);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
